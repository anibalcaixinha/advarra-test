require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'the password is stored encrypted' do
    user = User.find_by(username: 'some_user')
    assert user.encrypted_password != 'some_password'
    assert user.password == 'some_password'
  end

  test 'authentication works with the correct password' do
    user = User.find_by(username: 'some_user')
    assert user.authenticate('some_password')
  end

  test 'the login failures count increases' do
    user = User.find_by(username: 'some_user')
    assert user.login_failure_count == 0
    assert !user.authenticate('wrong_password')
    user.reload
    assert user.login_failure_count == 1
  end

  test 'the account is locked after max failed login attempts is reached' do
    user = User.find_by(username: 'some_user')
    assert user.login_failure_count == 0
    user.authenticate('wrong_password')
    user.authenticate('wrong_password')
    user.authenticate('wrong_password')

    assert user.login_failure_count == 3
    assert user.locked?
  end

  test 'the account login failures count is reset on a successfull login' do
    user = User.find_by(username: 'some_user')
    user.authenticate('wrong_password')
    user.authenticate('wrong_password')
    assert user.login_failure_count == 2
    user.authenticate('some_password')
    assert user.login_failure_count == 0
    user.reload
    # just make sure the actual value in the database is ok
    assert user.login_failure_count == 0
  end

  test 'the account locked even with concurrent requests' do
    user1 = User.find_by(username: 'some_user')
    user2 = User.find_by(username: 'some_user')
    # both instances have matching counters
    assert user1.login_failure_count == 0
    assert user2.login_failure_count == 0

    user1.authenticate('wrong_password')
    user2.authenticate('wrong_password')

    assert user1.login_failure_count == 1
    assert user2.login_failure_count == 1

    assert !user1.locked?
    assert !user2.locked?

    # local counters increment independently
    user1.authenticate('wrong_password')
    assert user1.login_failure_count == 2
    assert user2.login_failure_count == 1

    # locked? always knows the real value
    assert user1.locked?
    assert user2.locked?
    # lets fetch the same user again and compare the actual value
    user3 = User.find_by(username: 'some_user')
    assert user3.login_failure_count == 3
  end

  test 'validate username presence' do
    user = User.new(password: 'some_password')
    refute user.valid?
    assert_not_nil user.errors[:username]
  end

  test 'validate password presence' do
    user = User.new
    refute user.valid?
    assert_not_nil user.errors[:password]
  end
end
