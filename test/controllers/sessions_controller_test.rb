require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test 'should get new' do
    get new_session_url
    assert_response :success
  end

  test 'should create session' do
    post sessions_url, params: { session: { password: 'some_password', username: 'some_user' } }
    assert session[:username] == 'some_user'
    assert_redirected_to root_path
  end

  test 'should fail on missing password' do
    post sessions_url, params: { session: { password: '', username: 'some_user' } }
    assert session[:username].blank?
    assert_response 422
  end

  test 'should fail on missing username' do
    post sessions_url, params: { session: { password: 'some_password', username: '' } }
    assert session[:username].blank?
    assert_response 422
  end

  test 'should fail to create session on wrong password' do
    post sessions_url, params: { session: { password: 'wrong_password', username: 'some_user' } }
    assert session[:username].blank?
    assert_equal 'Invalid username or password', flash[:alert]
    assert_redirected_to root_path
  end

  test 'should fail to create session on wrong user' do
    post sessions_url, params: { session: { password: 'wrong_password', username: 'wrong_user' } }
    assert session[:username].blank?
    assert_redirected_to root_path
  end

  test 'should destroy the session on logout' do
    post sessions_url, params: { session: { password: 'some_password', username: 'some_user' } }
    assert session[:username] == 'some_user'

    delete session_url(session[:user_id])
    assert session[:username].blank?
    assert_redirected_to root_path
  end

  test 'should lock the account, on third fail' do
    user_name = 'some_user'
    post sessions_url, params: { session: { password: 'wrong_password', username: user_name } }
    post sessions_url, params: { session: { password: 'wrong_password', username: user_name } }
    post sessions_url, params: { session: { password: 'wrong_password', username: user_name } }
    assert session[:username].blank?
    assert_redirected_to root_path
    assert_equal "Invalid password, #{user_name} your account is now locked! please contact support!",
                 flash[:alert]
  end

  test 'should fail login on a locked account' do
    user_name = 'locked_user'
    post sessions_url, params: { session: { password: 'wrong_password', username: user_name } }
    assert session[:username].blank?
    assert_redirected_to root_path
    assert_equal "#{user_name} your account is locked! please contact support!", flash[:alert]
  end
end
