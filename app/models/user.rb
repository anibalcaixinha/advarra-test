require 'bcrypt'

class User < ApplicationRecord
  include BCrypt
  # usually should be loaded from yaml config file
  MAX_LOGIN_ATTEMPTS = 3

  validates :username, presence: true
  validates :password, presence: true

  def password
    @password ||= encrypted_password.present? ? Password.new(encrypted_password) : nil
  end

  def password=(new_password)
    if new_password.present?
      @password = Password.create(new_password)
      self.encrypted_password = @password
    else
      self.encrypted_password = nil
    end
  end

  def authenticate(login_password)
    if login_password.present? && password == login_password
      reset_login_failure_count!
      true
    else
      increment_login_failure_count!
      false
    end
  end

  def locked?
    # to make sure we are getting the correct number of failed attempts,
    # we read from the database directly
    # NOTE: as is, login_failure_count should never be read from directly
    # we could go as far as to overide the login_failure_count accessor
    current_count = User.where(id:).pluck(:login_failure_count).first || 0
    current_count >= MAX_LOGIN_ATTEMPTS
  end

  private

  def increment_login_failure_count!
    # make sure we do unitary increments
    # increment_counter will update the field login_failure_count on the user table directly
    self.class.increment_counter(:login_failure_count, id)
    # we will increment the in memmory counter aswell, but it's not as reliable
    increment(:login_failure_count)
    login_failure_count
  end

  def reset_login_failure_count!
    # set the counter to 0 directly in the database
    update_column(:login_failure_count, 0)
    self.login_failure_count = 0
    login_failure_count
  end
end
