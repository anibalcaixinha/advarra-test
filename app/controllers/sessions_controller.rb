class SessionsController < ApplicationController
  def new
    @session_form = Session.new
  end

  def create
    @session_form = Session.new(session_params)
    if @session_form.valid?
      login(@session_form.username, @session_form.password)
      redirect_to root_url
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    reset_session
    redirect_to root_path, notice: 'Signed out successfully!'
  end

  private

  # #login can be pulled into a service object
  def login(username, password)
    user = User.find_by(username:)
    if user.blank?
      flash[:alert] = 'Invalid username or password'
    elsif user.locked?
      flash[:alert] = "#{username} your account is locked! please contact support!"
    elsif user.authenticate(password)
      session[:user_id] = user.id
      session[:username] = user.username
      flash[:notice] = "Welcome back, #{username}"
    elsif user.locked?
      flash[:alert] = "Invalid password, #{username} your account is now locked! please contact support!"
    else
      flash[:alert] = 'Invalid username or password'
    end
  end

  # Only allow a list of trusted parameters through.
  def session_params
    params.require(:session).permit(:username, :password)
  end
end
