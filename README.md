# README

This README covers the steps necessary to run application and the test suite.

* Ruby version
    - 3.1.0p0

* System dependencies
    - Dependencies are manged through bundler, having the correct ruby version and bundler installed. Run:
    - bundle install

* Configuration
- Database configuration file: `config/database.yml`

* Database creation
- The configured database is SQlite3, this is appropriate for testing and demonstration purposes only

* Database initialization
- create the database
    - bin/rails db:create
- Insert users from seed file
    - rails db:seed 

* How to run the test suite
- The test suite can be ran with the command:
    - bin/rails test
    
* Test user
    - user_name: 'some_user', password: 'some_password'